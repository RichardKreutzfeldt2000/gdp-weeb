package ueb10;

public class Element {
    private int value;
    private Element next;

    //Methoden der Klasse
    public int getValue() {
        return value;
    }

    public void setValue (int value) {
        this.value = value;
    }

    public Element getNext() {
        return next;
    }

    public void setNext(Element next) {
        this.next = next;
    }

    public Element appendElement(int value) {
        if (this.next == null) {
            Element newElement = new Element();
            newElement.setValue(value);
            this.next = newElement;
        }
        else
            this.next = this.next.appendElement(value);
        return this;
    }

    public Element insertElement(int value) {
        if (this.value >= value) {
            Element newElement = new Element();
            newElement.setValue(value);
            newElement.setNext(this);
            return newElement;
        } else if (this.next == null) {
            Element newElement = new Element();
            newElement.setValue(value);
            this.next = newElement;
            return this;
        } else {
            this.next = this.next.insertElement(value);
            return this;
        }
    }

    public Element deleteElement(int value) {
        if (this.value == value) {
            return this.next;
        }
        else {
            if (this.next != null)
                this.next = this.next.deleteElement(value);
            return this;
        }
    }

    public void printList() {
        System.out.println (value);
        if (this.next != null)
            this.next.printList();
    }

    /**
     * @return die Anzahl der Elemente
     *
     */
    public int size() {
        if(this.next != null){
            return 1 + this.next.size();
        }
        return 1;
    }

    /**
     * @return die Summe der Werte dieses und aller folgenden Elemente
     *
     */
    public int sum() {
        if(this.next != null){
            return this.value + this.next.sum();
        }
        return this.value;
    }

    /**
     * @return true, wenn kein Element folgt oder die folgenden Elemente jeweils keinen kleineren Wert enthalten als ihr Vorgänger
     */
    public boolean isSorted() {
        if (this.next == null) {
            return true;
        } else if (this.value <= this.next.getValue()) {
            return this.next.isSorted();
        }
        return false;
    }

    /**
     * @param value der zu suchende Wert
     * @return true, wenn dieses oder eines der folgenden Elemente den übergebenen Wert enthält.
     *
     */
    boolean existsElement(int value) {
        if (this.value != value) {
            if (this.next == null) {
                return false;
            }
            return this.next.existsElement(value);
        }
        return true;
    }

    /**
     * Liefert einen String mit diesem Wert und jeweils durch ein Leerzeichen getrennt alle folgenden Werte. Um einen
     * Integer-Wert in einen String zu wandeln, kann man String.valueOf(value) benutzen, oder die implizite Wandlung mit
     * "" + value verwenden.
     *
     * @return String mit diesem Wert und jeweils durch ein Leerzeichen getrennt alle folgenden Werte
     *
     */
    String showValues() {
        if(this.next != null) {
            return value + " " + this.next.showValues();
        }
        return String.valueOf(value);
    }

    /**
     * Liefert den Wert des Elements an der Stelle x (Zählung beginnt bei 0). Wurde ein falscher Index angegeben, wird
     * Integer.MAX_VALUE zurückgegeben.
     *
     * @param index Index des Wertes
     * @return der Wert an der Stelle index
     *
     */
    int getValueAt(int index) {
        if(this.size() <= index || index < 0){
            return Integer.MAX_VALUE;
        }
        if(index == 0){
            return this.value;
        }
        return this.next.getValueAt(--index);
    }

    /**
     * Fügt ein neues Element mit dem übergebenen Wert an der Stelle index ein. Wird als Index die Arraylänge übergeben,
     * so wird das neue Element angehängt. Wurde ein falscher Index übergeben, so wird das Element und die folgenden
     * unverändert zurückgegeben.
     *
     * @param value neuer Wert
     * @param index Index zum Einfügen des neuen Werts
     * @return der Rest der Liste
     */
    Element insertElementAt(int value, int index) {
        if(index > this.size() || index < 0){
            return this;
        }
        if(index == this.size()){
            return this.appendElement(value);
        }
        if(index == 0){
            return this.insertElementAtFront(value);
        }
        if(this.next.insertElementAt(value, --index) != null){
            return this;
        }
        return this;
    }

    Element copyElement(Element newElement, int counter){
        if(counter == 0){
            newElement.setValue(this.value);
        }else {
            newElement.appendElement(this.value);
        }
        if(this.next != null){
            return this.next.copyElement(newElement, ++counter);
        }
        return newElement;
    }

    /**
     * Fügt ein neues Element mit dem übergebenen Wert vor das aktuelle Element ein.
     *
     * @param value neuer Wert
     * @return der Rest der Liste
     */
    Element insertElementAtFront(int value) {
        Element old = this.copyElement(new Element(), 0);
        this.value = value;
        this.next = old;
        return this;
    }
}
